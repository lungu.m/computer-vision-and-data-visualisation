"""
This script enables visualisation of data recorded by the webcam motion detector in an interactive Bokeh graph, using 
bokeh web based live plotting tools
"""
#import necessary packages
from webcamMotionDetect import df 
from bokeh.plotting import figure, show, output_file
from bokeh.models import HoverTool, ColumnDataSource

#convert data frame values to strings
df["Start_str"]=df["Start"].dt.strftime("%Y-%m-%d %H:%M:%S")
df["End_str"]=df["End"].dt.strftime("%Y-%m-%d %H:%M:%S")

# creating columns of dataframe compatible with bokeh graphing
cds=ColumnDataSource(df) 

#create the figure object and set parameters
#remove subdivisions on the y axis and eliminate unnecessary grid lines
f=figure(x_axis_type='datetime', height=100, width=500, sizing_mode= 'scale_width', title= "Motion graph")
f.yaxis.minor_tick_line_color=None 
f.ygrid[0].ticker.desired_num_ticks=1 

#initialize hovering tools so that we can hover around on data and add them to the figure object
hover=HoverTool(tooltips=[("Start", "@Start_str"),("End", "@End_str")]) # initializing hower tools so that we can hover around on data
f.add_tools(hover) 

#generate  a quad graph
q=f.quad(left="Start", right="End", bottom=0, top=1, color="green", source=cds) 

#save the the output files as an html code
output_file("Graph.html")
show(f)