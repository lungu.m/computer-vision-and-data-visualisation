#import the necessary packages
import numpy as np 
import cv2, time, pandas
from datetime import datetime
"""
set objects
"""
#set the first frame to capture a static image, initiate the status list and times list
#create a pandas data frame to collect the times activities from the camera
#triger the camera
first_frame=None  
status_list=[None, None]
times=[]
df=pandas.DataFrame(columns=["Start","End"])

video=cv2.VideoCapture(0)  

#fourcc=cv2.VideoWriter_fourcc(*'MJPG')
rvideo=cv2.VideoWriter('recorded_event.mp4',-1, 20.0, (640, 480))


"""
main loop
"""
#Here we loop over and over the frame to create a continous video recording
while True:
    #Check the presence of the camera and initialize the video frame
    #flip the frame 180, else the video will be inverted
    #set the camera status to zero and frame status text "unoccupied"
    check, frame=video.read()
    frame=cv2.flip(frame, 1)
    print(check) 
    print(frame) 
    status=0
    text="Unoccupied"
    
    #convert the color frame to gray scale and blur it
    gray=cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray=cv2.GaussianBlur(gray,(21,21),0)
    
    # the condition execute to capture a static image in the absence of motion
    if first_frame is None: 
        first_frame=gray
        cv2.imwrite("Static image.jpg", first_frame)
        continue
    
    #if frame != first_frame:
    #    rvideo.write(frame)

    #We create the delta frame which finds the difference between the first frame (static) and the current frame  (gray) by computing the 
    #absolute differnce between the two frames
    #Create a threshold frame and dilate it to eliminate noise due to shadows of the object.
    delta_frame=cv2.absdiff(first_frame, gray) 
    thresh_frame=cv2.threshold(delta_frame, 30, 255, cv2.THRESH_BINARY)[1]
    thresh_frame=cv2.dilate(thresh_frame, None, iterations=5) 
    
    #find contours in the threshold frame 
    (cnts,_)=cv2.findContours(thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) # create the contour around an object
    
    #loop through and compute the bounding box which is drawn around the object
    for contour in cnts:
        if cv2.contourArea(contour) < 10000:
            continue
        status=1
        (x,y,w,h)=cv2.boundingRect(contour)
        cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 3)
        text="Occupied"
        rvideo.write(frame)

    #Adding the status and timestamp to the frame
    cv2.putText(frame, "Camera Status: {}".format(text), (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0 ,0, 255),3)
    cv2.putText(frame, datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),(10,frame.shape[0]-10), cv2.FONT_HERSHEY_SIMPLEX,0.35,(0,0,255),2)

    #append the camera status to the status list and updating the list to only print out the last 2 iterms to scrap on memory usage
    status_list.append(status) 
    status_list=status_list[-2:] 

    #creating the time list for occupied frame
    if status_list[-1]==1 and status_list[-2]==0:
        times.append(datetime.now())
    if status_list[-1]==0 and status_list[-2]==1:
        times.append(datetime.now())
    
    #show the windows for the different frames
    cv2.imshow("Gray frame", gray)
    cv2.imshow("Delta frame", delta_frame)
    cv2.imshow("Threshold frame", thresh_frame)
    cv2.imshow("Camera feed", frame)

    key=cv2.waitKey(1) 

    #the program is set to terminate execution when the 'q' is pressed
    if key==ord('q'):
        if status==1:
            times.append(datetime.now())
        break
    

print(status_list)
print(times)

#create a pandas data frame for data analysis of the activities pertaining to the camera status and saving it to a csv file for analysis in excel
for i in range(0, len(times), 2):
    df=df.append({"Start":times[i], "End":times[i+1]}, ignore_index=True)
    

#df.to_excel("times.xlsx") 
df.to_csv("times.csv") 

#disabling the camera and close all open windows
video.release()
cv2.destroyAllWindows